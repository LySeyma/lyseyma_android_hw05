package com.krd.homeeork05;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home extends Fragment implements Add_Dailog.DialogCallback<String> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Home() {
        // Required empty public constructor
    }
    RecyclerView recyclerView;
    List<Book_model> list_books;
    public EditText edt_title,edt_size,edt_price;
    ImageView img_moreAction;
    private int counter = 0;
//    Resources res = getResources();
//    int resourceId = res.getIdentifier("stephen", "drawable", );


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment home.
     */
    // TODO: Rename and change types and number of parameters
    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        recyclerView = view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.HORIZONTAL));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.onCreate(null);
        getData();
    }
    private void getData(){
        list_books = new ArrayList<>();
        list_books = BookDatabase.getDatabase(getContext()).getBookDao().findAllBook();
        recyclerView.setAdapter( new MyAdater(getContext(), list_books, new MyAdater.DeleteBook() {
            @Override
            public void onBookDelete(int position, int id) {
                BookDatabase.getDatabase(getContext()).getBookDao().deleteBook(id);

            }
        },getFragmentManager()));

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.top_navigation_menu , menu);

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.add:
                insertDialog();

                return true;
            case R.id.search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private List<Book_model> initData() {
        list_books = new ArrayList<>();
        return list_books;
    }
    public void insertDialog() {
        Add_Dailog exampleDialog = new Add_Dailog();
        exampleDialog.show(getFragmentManager(),"insert book");
        exampleDialog.setCallback(this);

    }

//    @Override
//    public void applyTexts(String title, String size, String price) {
//        edt_price.setText(price);
//        edt_size.setText(size);
//        edt_title.setText(title);
//    }

    @Override
    public void onClicked(String data) {

    }
}
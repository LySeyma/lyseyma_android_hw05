package com.krd.homeeork05;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdater extends RecyclerView.Adapter<MyAdater.ViewHolder> {

    private List<Book_model> list_books;
    private Context context;
    MainActivity mainActivity;

    DeleteBook deleteBook;
    FragmentManager FragManager ;

    public MyAdater(Context context,List<Book_model> list_books,DeleteBook deleteBook,FragmentManager getSupportFragmentManager){
        this.context=context;
        this.list_books =list_books;
        FragManager = getSupportFragmentManager;
        this.deleteBook = deleteBook;
    }

    @NonNull
    @Override
    public MyAdater.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_book,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyAdater.ViewHolder holder, final int position) {
        holder.txt_title.setText(list_books.get(position).getTitle());
        holder.txt_category.setText(list_books.get(position).getCategory());
//        holder.img_book.setImageResource(list_books.get(position).getImgId());
//        holder.txt_size.setText(Double.toString(list_books.get(position).getSize()));
//        holder.txt_price.setText(Double.toString(list_books.get(position).getSize()));
//        ((MainActivity)context).resetGraph(context);
        holder.txt_size.setText(list_books.get(position).getSize());
        holder.txt_price.setText(list_books.get(position).getPrice());
       holder.img_moreAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                        case R.id.update:
                            Bundle args = new Bundle();
                            args.putString("id",String.valueOf(list_books.get(position).getId()));
                            args.putString("image",String.valueOf(list_books.get(position).getImage()));
                            args.putString("title",String.valueOf(list_books.get(position).getTitle()));
                            args.putString("size",String.valueOf(list_books.get(position).getSize()));
                            args.putString("price",String.valueOf(list_books.get(position).getPrice()));
                             DialogFragment update_dialog = new Update_Dialog();
                             update_dialog.setArguments(args);
                             update_dialog.show(FragManager ,"TAG");
                            return true;
                        case R.id.delete:
                            Toast.makeText(context, "Delete Successfully", Toast.LENGTH_SHORT).show();
                            deleteBook.onBookDelete(position,list_books.get(position).getId());
                            return true;
                        case R.id.read:
                            Intent intent = new Intent(context,Read.class);
                            intent.putExtra("image",String.valueOf(list_books.get(position).getImage()));
                            intent.putExtra("title",String.valueOf(list_books.get(position).getTitle()));
                            intent.putExtra("size",String.valueOf(list_books.get(position).getSize()));
                            intent.putExtra("price",String.valueOf(list_books.get(position).getPrice()));
                            intent.putExtra("category",String.valueOf(list_books.get(position).getCategory()));
                            context.startActivity(intent);
                            return true;
                        default:
                            return false;
                    }
                    }
                });
                popupMenu.inflate(R.menu.more_menu);
                popupMenu.show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return list_books.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView img_book,img_moreAction;
        public TextView txt_title,txt_category,txt_size,txt_price;
        public GridLayout gridLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_book = itemView.findViewById(R.id.img_book);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_category = itemView.findViewById(R.id.txt_category);
            txt_size = itemView.findViewById(R.id.txt_size);
            txt_price = itemView.findViewById(R.id.txt_price);
            img_moreAction = itemView.findViewById(R.id.img_more_menu);
            gridLayout = itemView.findViewById(R.id.gridlayout);


        }
    }
    public interface DeleteBook{
        void onBookDelete(int position,int id);
    }

}

package com.krd.homeeork05;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "book")
public class Book_model {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    public Uri image ;
    public String title,category;
    public String size;
    public String price;

    public Book_model(){

    }

    public Book_model(Uri image,String title, String category, String size, String price) {

        this.image= image;
        this.title = title;
        this.category = category;
        this.size = size;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "book_model{" +
                ", title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", size=" + size +
                ", price=" + price +
                '}';
    }
}

package com.krd.homeeork05;

import android.Manifest;
import android.app.AppComponentFactory;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class Add_Dailog extends DialogFragment {
    public EditText edt_title,edt_size,edt_price;
    private ImageView img_book;
    public ImageView img_add,img_add_book;
    Spinner spinner;
    private static final int PERMISSION_CODE = 1001;
    static final int SELECT_IMAGE = 1000;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.add_book, null);
        edt_price = view.findViewById(R.id.edt_price);
        edt_size = view.findViewById(R.id.edt_size);
        edt_title = view.findViewById(R.id.edt_title);
        spinner = view.findViewById(R.id.sp_category);
        img_book = view.findViewById(R.id.img_book);
        img_add= view.findViewById(R.id.img_add_img);
        img_add_book= view.findViewById(R.id.img_add_book);

        builder.setTitle("Insert Book");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                        String title = edt_title.getText().toString().trim();
                        String size = edt_size.getText().toString().trim();
                        String price = edt_price.getText().toString().trim();
                        Uri image = Uri.parse(img_add_book.toString().trim());
                        ArrayAdapter<String> spinnerView = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.category));
                        spinnerView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(spinnerView);
                            Book_model bookModel = new Book_model();
                            bookModel.setTitle(title);
                            bookModel.setPrice(price);
                            bookModel.setSize(size);
                            bookModel.setImage(image);
                            BookDatabase.getDatabase(getContext()).getBookDao().insertBook(bookModel);
                            edt_title.setText("");
                            edt_price.setText("");
                            edt_size.setText("");
                            img_add_book.setImageURI(image);
                            Toast.makeText(getContext(),"Add Book",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        builder.setView(view);
       img_add.setOnClickListener(new View.OnClickListener() {
//            @Override
            public void onClick(View v) {
                pickImageFromGallery();
            }
        });

        return builder.create();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        try {
////            listener = (DialogListener) context;
////        } catch (ClassCastException e) {
////            throw new ClassCastException(context.toString() +
////                    "must implement DialogListener");
////        }
    }


    public interface DialogListener {
        void applyTexts(String title, String size, String price);
    }
    private void pickImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"add Image"),SELECT_IMAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSION_CODE:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_DENIED){
                    pickImageFromGallery();
                }
                else {
                    Toast.makeText(getContext(),"Permission demied...",Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
//        if (requestCode == GALLERY_REQUEST_CODE && requestCode == RESULT_OK ) {
//            Uri imageData = data.getData();
//            img_add_book.setImageURI(imageData);
//
//        }
//
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (resultCode == RESULT_OK){
                    if (requestCode == SELECT_IMAGE){
                        final Uri addImageUri = data.getData();
                        if (null != addImageUri){
                            img_add_book.post(new Runnable() {
                                @Override
                                public void run() {
                                    img_add_book.setImageURI(addImageUri);
                                }
                            });
                        }
                    }
                }

            }
        }).start();
    }
    public DialogCallback callback;
    public void setCallback(DialogCallback callback) {
        this.callback = callback;
    }


    public interface DialogCallback<T>{
        void onClicked(T data);
    }
}

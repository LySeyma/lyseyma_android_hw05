package com.krd.homeeork05;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class Update_Dialog extends DialogFragment {
    public EditText edt_title,edt_size,edt_price;
    private ImageView img_book;
    public ImageView img_add,img_add_book;
    String[] category = {"Donate", "Sell"};
    Spinner spinner;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.add_book, null);
        edt_price = view.findViewById(R.id.edt_price);
        edt_size = view.findViewById(R.id.edt_size);
        edt_title = view.findViewById(R.id.edt_title);
        spinner = view.findViewById(R.id.sp_category);
        img_book = view.findViewById(R.id.img_book);
        img_add= view.findViewById(R.id.img_add_img);
        img_add_book= view.findViewById(R.id.img_add_book);

        Bundle mArgs = getArguments();

        builder.setView(view);
//        Bundle extras = getIntent().getExtras();
////        edt_title.setText(getI)
        Bundle bundle = getArguments();
        String title = bundle.getString("title");
        String size = bundle.getString("size");
        String price = bundle.getString("price");

        edt_title.setText(title);
        edt_size.setText(size);
        edt_price.setText(price);
        final String id = bundle.getString("id");
        builder.setTitle("Update Book");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title = edt_title.getText().toString().trim();
                String size = edt_size.getText().toString().trim();
                String price = edt_price.getText().toString().trim();
                ArrayAdapter spinnerView = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,category);
                spinnerView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(spinnerView);
                BookDatabase.getDatabase(getContext()).getBookDao().updateBook(title,size,price,Integer.parseInt(id));
                Toast.makeText(getContext(),"Update",Toast.LENGTH_LONG).show();
                dialog.dismiss();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });




        return builder.create();
    }


    public void show(Intent intent, String s) {
    }
}

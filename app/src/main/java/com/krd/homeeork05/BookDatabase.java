package com.krd.homeeork05;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@TypeConverters({UriConverters.class})
@Database(entities = {Book_model.class}, version = 2)
public abstract class BookDatabase extends RoomDatabase {

    public abstract BookDao getBookDao();
    private static BookDatabase bookDatabase;
    static BookDatabase getDatabase(final Context context){
    if (bookDatabase == null){
    synchronized (BookDatabase.class){
        if (bookDatabase == null){
            bookDatabase = Room.databaseBuilder(context.getApplicationContext(),BookDatabase.class,"Book_Database").allowMainThreadQueries().addMigrations(MIGRATION_1_2).build();

        }
    }

    }
    return bookDatabase;
}
    public static final Migration MIGRATION_1_2 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE book "
                    + " ADD COLUMN image Uri");
        }
    };

}

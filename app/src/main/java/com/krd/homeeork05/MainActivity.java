package com.krd.homeeork05;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener   {

    private static final String TAG = "MainActivity";
    BottomNavigationView bottomNavigationView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.bottom_navigation);


        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        FragmentManager manager = this.getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.my_frameLayout,new Home()).commit();

//        MyAdater = new

//        Intent refresh = new Intent(this, MainActivity. class); startActivity(refresh);

//        IntentFilter inF = new IntentFilter("data_changed");
//        LocalBroadcastManager.getInstance(this).registerReceiver(dataChangeReceiver,inF);

    }
//    private BroadcastReceiver dataChangeReceiver= new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // update your listview
//        }
//    };

//    @Override
//    protected void onPause() {
//        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(dataChangeReceiver);
//    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.i(TAG,"onNavigationItemSelected");
        FragmentManager manager = this.getSupportFragmentManager();
        int id = item.getItemId();
        switch (id) {
            case R.id.home:
                manager.beginTransaction()
                        .replace(R.id.my_frameLayout,new Home()).commit();
                return true;
            case R.id.dashboard:

                manager.beginTransaction()
                        .replace(R.id.my_frameLayout,new Dashboard()).commit();
                return true;
            case R.id.notificaton:
                manager.beginTransaction()
                        .replace(R.id.my_frameLayout,new Notification()).commit();
                return true;
            case R.id.upload:
                Toast.makeText(getApplicationContext(),"Upload",Toast.LENGTH_LONG).show();
                return true;
            case R.id.share:
                Toast.makeText(getApplicationContext(),"Share",Toast.LENGTH_LONG).show();
                return true;
            default:


        }
        return false;
    }
}
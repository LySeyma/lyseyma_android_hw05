package com.krd.homeeork05;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BookDao {

@Insert
void insertBook (Book_model bookModel);

@Query("select * from book")
    List<Book_model> findAllBook();

@Query("delete from book where `id`= :id " )
void deleteBook(int id );

@Query("update book set title = :title, size = :size,price = :price where `id` = :id")
    void updateBook(String title, String size, String price,int id);





}

package com.krd.homeeork05;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Read extends AppCompatActivity {
    TextView txt_title,txt_size,txt_price,txt_category;
    ImageView img_book;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        txt_title= findViewById(R.id.txt_title);
        txt_size = findViewById(R.id.txt_size);
        txt_price = findViewById(R.id.txt_price);
        txt_category = findViewById(R.id.txt_category);
        img_book = findViewById(R.id.image_book);
        btn_back= findViewById(R.id.btn_back);

        img_book.setImageURI(Uri.parse(getIntent().getExtras().getString("image")));
        txt_title.setText(getIntent().getExtras().getString("title"));
        txt_category.setText(getIntent().getExtras().getString("category"));
        txt_size.setText(getIntent().getExtras().getString("size"));
        txt_price.setText(getIntent().getExtras().getString("price"));
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
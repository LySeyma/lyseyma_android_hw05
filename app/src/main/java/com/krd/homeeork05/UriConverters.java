package com.krd.homeeork05;

import android.net.Uri;

import androidx.room.TypeConverter;

public class UriConverters {
    @TypeConverter
    public static Uri image(String value) {
        if (value == null){
            image(value);
        }
        else {
            Uri.parse(value);
        }
        return Uri.parse(value);
    }

    @TypeConverter
    public static String toString(Uri uri){
        return uri.toString();
    }
}
